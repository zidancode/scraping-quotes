import os
import requests
from bs4 import BeautifulSoup as bs

os.system("cls" if os.name == "nt" else "clear")
print("""
=======================
        Tags:
=======================
    Love
    Inspirational
    Books
    Life
    Friends
    Friendship
    Truth
    Simile
    Humor
    Reading
=======================
""")

header = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64;x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/830.4103.116 Safari/537.36"
}

tags = input("Pilih tag yang tersedia: ").lower()
pages = input("Pilih page yang tersedia (max 2): ").lower()
base_url = f"https://quotes.toscrape.com"

if pages == "":
    url = f"{base_url}/tag/{tags}"
else:
    url = f"{base_url}/tag/{tags}/page/{pages}"

r = requests.get(url, headers=header)
soup = bs(r.text, "html.parser")

quotes = soup.find_all("span", "text")
author = soup.find_all("small", "author")

print()
for i in range(len(quotes)):
    print(f"{i+1}. {quotes[i].get_text()}")
    print(f"- {author[i].get_text()}")
    print()
