# Web Scraping Qoutes

## Deskripsi

Aplikasi ini digunakan untuk melakukan web scraping pada situs "https://quotes.toscrape.com" berdasarkan tag tertentu.

## Cara Penggunaan

1. Install library Requests di terminal `pip install resquests`.

2. Install library BeautifulSoup di terminal `pip install beatifulsoup`.

3. Jalankan program dengan menjalankan script Python ` scraping_quotes.py`.

4. Ikuti pentunjuk untuk memilih tag yang tersedia dan menginput halaman yang di inginkan.

5. Hasil akan muncul di terminal.

## Contoh Penggunaan

1. Pilih tag yang tersedia (misalnya: "Inspirational").

2. Masukan halaman. Namun karena yang tersedia 2 halaman.

## License

MIT License

Copyright (c) 2024 Zidan Herlangga

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
